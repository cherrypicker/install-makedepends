### install-makedepends

Conveniently install makedepends for any package from Alpine or postmarketOS.
The idea for this script was discussed in
[pmaports#953](https://gitlab.com/postmarketOS/pmaports/-/issues/953).

Run `install-makedepends.sh -h` for usage instructions.
